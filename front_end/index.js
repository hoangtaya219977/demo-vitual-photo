const webcamElement = document.getElementById('webcam');
const canvasElement = document.getElementById('canvas');
const snapSoundElement = document.getElementById('snapSound');
const webcam = new Webcam(webcamElement, 'user', canvasElement, snapSoundElement);
context = canvasElement.getContext('2d');

function handleStartWebcam() {
    webcam.start()
        .then(result => {
            console.log("webcam started");
        })
        .catch(err => {
            console.log(err);
        });

}

function handleStopWebcam() {
    webcam.stop();
}

function handleSelfie() {
    webcam.snap();
}

function handleDownload() {
    const dataImage = canvasElement.toDataURL()
    console.log(dataImage)
    const imageDownload = document.createElement("a");
    document.body.appendChild(imageDownload);
    imageDownload.href = dataImage;
    imageDownload.download = "image.png";
    imageDownload.click();
}


// let currentX = canvasElement.width / 2, currentY = canvasElement.height / 2

function handleInsertIcon(idIcon) {

    var isDraggable = false;
    currentX = canvasElement.width / 2;
    currentY = canvasElement.height / 2;

    const icon = document.getElementById(idIcon);
    const star_img = new Image();
    star_img.crossOrigin = "Anonymous";
    star_img.src = icon.src



    context = canvasElement.getContext('2d');
    // context.drawImage(star_img, currentX - (star_img.width / 2), currentY - (star_img.height / 2));

    _Go()

    function _Go() {
        _MouseEvents();

        setInterval(function () {
            _DrawImage();
        }, 1000 / 0);
    }

    function _MouseEvents() {
        canvas.onmousedown = function (e) {
            var mouseX = e.pageX - this.offsetLeft;
            var mouseY = e.pageY - this.offsetTop;

            if (mouseX >= (currentX - star_img.width / 2) &&
                mouseX <= (currentX + star_img.width / 2) &&
                mouseY >= (currentY - star_img.height / 2) &&
                mouseY <= (currentY + star_img.height / 2)) {
                isDraggable = true;
                //currentX = mouseX;
                //currentY = mouseY;
            }
        };
        canvas.onmousemove = function (e) {

            if (isDraggable) {
                currentX = e.pageX - this.offsetLeft;
                currentY = e.pageY - this.offsetTop;
            }
        };
        canvas.onmouseup = function (e) {
            isDraggable = false;
        };
        canvas.onmouseout = function (e) {
            isDraggable = false;
        };
    }
    function _DrawImage() {
        context.drawImage(star_img, -(currentX - (star_img.width / 2)), (currentY - (star_img.height / 2)));
    }

}