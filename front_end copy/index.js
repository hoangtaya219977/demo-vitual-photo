const webcamElement = document.getElementById('webcam');
const canvasElement = document.getElementById('canvas');
const snapSoundElement = document.getElementById('snapSound');
const webcam = new Webcam(webcamElement, 'user', canvasElement, snapSoundElement);
const canvas12 = new fabric.Canvas('canvas', {
  width: 640,
  height: 480
});
let isWebcam = false;

function handleStartWebcam() {
  if (!isWebcam) {
    isWebcam = true;
    webcam.start()
      .then(result => {
        console.log("webcam started");
        isWebcam = true;
      })
      .catch(err => {
        console.log(err);
        isWebcam = false
      });
  }
}

function handleStopWebcam() {
  webcam.stop();
}

//handle selfie
let isHandleDisableSelfie = false;
function handleSelfie() {
  canvas12.clear().renderAll();
  if (isHandleDisableSelfie === false) {
    if (isWebcam) {
      isWebcam = false;
    }
    else {
      handleStartWebcam()
    }
    var counter = 6;
    var newYearCountdown = setInterval(function () {
      counter--
      document.getElementById("time-selfie").innerHTML = counter;
      if (counter === 0) {
        webcam.flip();
        var img = new Image();
        img.src = webcam.snap();
        img.onload = function () {
          var f_img = new fabric.Image(img);
          canvas12.setBackgroundImage(f_img);
          canvas12.renderAll();
        };
        handleStopWebcam();
        isWebcam = false;
        clearInterval(newYearCountdown);
        isHandleDisableSelfie = false;
      }
    }, 1000);
  }
  isHandleDisableSelfie = true;
}

//button clear item
function ResetIcon() {
  var object = canvas12.getActiveObject();
  canvas12.remove(object);
}

function handleDownload() {
  const dataImage = canvas12.toDataURL()
  const imageDownload = document.createElement("a");
  document.body.appendChild(imageDownload);
  imageDownload.href = dataImage;
  imageDownload.download = "image.png";
  imageDownload.click();
}

function handleInsertIcon(idIcon) {
  const icon = document.getElementById(idIcon);
  const star_img = new Image({});
  star_img.crossOrigin = "Anonymous";
  star_img.src = icon.src

  star_img.onload = function () {
    var iconInsert = new fabric.Image(star_img, {
      left: 50,
      top: 70,
    });
    canvas12.add(iconInsert);
  };
}

function handleRemoteIcon() {
  var object = canvas12.getActiveObject();
  if (object) {
    canvas12.remove(object);
  }
}

$('.carousel-slick .slider-nav')
  .slick({
    slidesToShow: 4,
    slidesToScroll: 4,
    dots: false,
    centerMode: true,
    focusOnSelect: true,
    prevArrow: false,
    nextArrow: false,
    responsive: [{
      breakpoint: 1024,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 5,
      }
    }, {
      breakpoint: 640,
      settings: {
        slidesToShow: 4,
        slidesToScroll: 4,
      }
    }, {
      breakpoint: 420,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3,
      }
    }]
  });
